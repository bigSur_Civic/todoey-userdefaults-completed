//
//  Item.swift
//  Todoey
//
//  Created by Warbi Võ  on 05/03/2022.
//  Copyright © 2022 Warbi Võ . All rights reserved.
//

import Foundation
class Item: Codable {
    var title: String = ""
    var done: Bool = false
}
